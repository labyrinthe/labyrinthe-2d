
#include "unionfind.h"

// from http://fr.wikipedia.org/wiki/Union-Find

void makeset(uf_t* puf)
{
    puf->parent = puf;
    puf->rank = 0;
}

void unionset(uf_t* pufx, uf_t* pufy)
{
    uf_t* xroot = find(pufx);
    uf_t* yroot = find(pufy);

    if (xroot == yroot)
        return;
    else if (xroot->rank < yroot->rank)
        xroot->parent = yroot;
    else if (xroot->rank > yroot->rank)
        yroot->parent = xroot;
    else
    {
        yroot->parent = xroot;
        xroot->rank++;
    }
}

uf_t* find(uf_t* pufx)
{
    if (pufx->parent != pufx)
        pufx->parent = find(pufx->parent);
    return pufx->parent;
}

