#ifndef _ALEATOIRE_H
#define _ALEATOIRE_H

int InitGenerateur (int min, int max);
int ReturnAleatoire (void);
void TermineGenerateur (void);

#endif
