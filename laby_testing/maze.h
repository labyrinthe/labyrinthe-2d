#ifndef _MAZE_H_
#define _MAZE_H_

#include <cstdlib>


#define MUR_G   (1<<0)
#define MUR_D   (1<<1)
#define MUR_H   (1<<2)
#define MUR_B   (1<<3)
#define FERME   (MUR_G|MUR_D|MUR_H|MUR_B)

/* union_find */
struct uf_t
{
    struct uf_t* parent;
    size_t rank;
};

struct pathinfo_t
{
    bool visited : 1;
    bool path : 1;
    size_t prev_idx;
};

/* Structure contenant les parametres du labyrinthe */
typedef struct
{
    size_t largeur, hauteur;
    unsigned char* flag;     /* etat de la cellule (murs) */
    pathinfo_t* info;
    uf_t* uf;     /* enregistre si la cellule est reliee au labyrinthe */
} Maze;

struct wall_t
{
    wall_t(size_t i, unsigned char f) : idx(i), flag(f) {}
    size_t idx;
    unsigned char flag;
};



Maze* CreateMaze (size_t x, size_t y);
void GeneratePerfect2D_Maze (Maze* lab);
void DeleteMaze (Maze* lab);

bool RechercheChemin (Maze* lab, size_t depart, size_t arrivee);
void ResetInfos (Maze* lab);
void ExportTo (Maze* lab, const char* outfilename, bool markpath = false);
#endif
