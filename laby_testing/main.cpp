#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>

using namespace std;

#include "maze.h"

#define DEFAULT_X   60//0//0
#define DEFAULT_Y   40//0//0

int main()
{
    srand ((unsigned)time(NULL));

    char out[52];

    Maze *lab = NULL;
    if ( (lab = CreateMaze (DEFAULT_X, DEFAULT_Y)) != NULL )
    {
        for (int n = 1; n <= 100; ++n)
        {
            clock_t t1 = clock();

            GeneratePerfect2D_Maze (lab);

            clock_t t2 = clock();

            cout << "create the " << n << "th maze" << endl;
            cout << " generated in " << (double)(t2-t1) / CLOCKS_PER_SEC << "s" << endl;

            sprintf(out, "out%2d.png", n);
            ExportTo (lab, out);

            sprintf(out, "out%2d solved.png", n);
            ExportTo (lab, out, true);

            //system("PAUSE");

        }
    }

    DeleteMaze(lab);

    return 0;
}
