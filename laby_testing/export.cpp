#include "maze.h"

#include <iostream>

using namespace std;

/////////////////////////////////////////////////
//          export-to-png function
/////////////////////////////////////////////////
#include <cairo/cairo.h>


#define SZ_CELL_EXPORT  6


#define MARGIN      10
#define MARGIN_2    5


/// TODO
// les bord ne sont pas arrondis...
void ExportTo (Maze* lab, const char* outfilename, bool markpath)
{
    cairo_surface_t *surface;
    cairo_t *cr;

    surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, SZ_CELL_EXPORT * lab->largeur + MARGIN, SZ_CELL_EXPORT * lab->hauteur + MARGIN);
    cr = cairo_create (surface);

    cairo_set_source_rgb (cr, 1., 1., 1.); /* BLANC */
    cairo_paint (cr);

    if (markpath && RechercheChemin(lab, 0, lab->largeur * lab->hauteur - 1))
    {
        cairo_set_source_rgb (cr, 1., 0.9, 0.); /* JAUNE */

        size_t idx = 0;

        cairo_move_to(cr, MARGIN_2 + SZ_CELL_EXPORT / 2, MARGIN_2 + SZ_CELL_EXPORT / 2);

        while (idx != lab->largeur * lab->hauteur - 1)
        {
            if ( !(lab->flag[idx] & MUR_G) && lab->info[idx-1].path)
            {
                cairo_rel_line_to(cr, -SZ_CELL_EXPORT, 0);
                idx--;
            }
            else if ( !(lab->flag[idx] & MUR_D) && lab->info[idx+1].path)
            {
                cairo_rel_line_to(cr, SZ_CELL_EXPORT, 0);
                idx++;
            }
            else if ( !(lab->flag[idx] & MUR_H) && lab->info[idx-lab->largeur].path)
            {
                cairo_rel_line_to(cr, 0, -SZ_CELL_EXPORT);
                idx-=lab->largeur;
            }
            else if ( !(lab->flag[idx] & MUR_B) && lab->info[idx+lab->largeur].path)
            {
                cairo_rel_line_to(cr, 0, SZ_CELL_EXPORT);
                idx+=lab->largeur;
            }
            lab->info[idx].path = false;
        }

        cairo_set_line_width (cr, SZ_CELL_EXPORT - 1);
        cairo_stroke (cr);
    }
    else if (markpath)
    {
        cout << "no path found !" << endl;
        return;
    }

    {
        cairo_set_source_rgb (cr, 0., 0., 0.); /* NOIR */

#define cairo_move_to(c,x,y)    cairo_move_to(c,x+MARGIN_2,y+MARGIN_2)
#define cairo_line_to(c,x,y)    cairo_line_to(c,x+MARGIN_2,y+MARGIN_2)

        size_t x, y, idx;
        /* mur superieur - 1e ligne ; simplifiable */
        bool start = false;
        for (x = 0, idx = 0; x < lab->largeur; ++x, ++idx)
        {
            if (lab->flag[idx] & MUR_H)
            {
                if (!start)
                {
                    cairo_move_to (cr, x * SZ_CELL_EXPORT, 0);
                    start = true;
                }
            }
            else
            {
                if (start) // fin d'une ligne, on trace
                {
                    cairo_line_to(cr, x * SZ_CELL_EXPORT, 0);
                    start = false;
                }
            }
        }
        if (start) // fin d'une ligne, on trace
        {
            cairo_line_to(cr, x * SZ_CELL_EXPORT, 0);
        }

        /* parcours par lignes */
        for (y = 1, idx = 0; y <= lab->hauteur; ++y)
        {
            start = false;
            for (x = 0; x < lab->largeur; ++x, ++idx)
            {
                if (lab->flag[idx] & MUR_B)
                {
                    if (!start)
                    {
                        cairo_move_to (cr, x * SZ_CELL_EXPORT, y * SZ_CELL_EXPORT);
                        start = true;
                    }
                }
                else
                {
                    if (start) // fin d'une ligne, on trace
                    {
                        cairo_line_to(cr, x * SZ_CELL_EXPORT, y * SZ_CELL_EXPORT);
                        start = false;
                    }
                }
            }
            if (start) // fin d'une ligne, on trace
            {
                cairo_line_to(cr, x * SZ_CELL_EXPORT, y * SZ_CELL_EXPORT);
            }
        }

        /* mur gauche - 1e colonne ; simplifiable */
        start = false;
        for (y = 0, idx = 0; y < lab->hauteur; ++y, idx+=lab->largeur)
        {
            if (lab->flag[idx] & MUR_G)
            {
                if (!start)
                {
                    cairo_move_to (cr, 0, y * SZ_CELL_EXPORT);
                    start = true;
                }
            }
            else
            {
                if (start) // fin d'une ligne, on trace
                {
                    cairo_line_to(cr, 0, y * SZ_CELL_EXPORT);
                    start = false;
                }
            }
        }
        if (start) // fin d'une ligne, on trace
        {
            cairo_line_to(cr, 0, y * SZ_CELL_EXPORT);
        }

        /* parcours par colonnes */
        for (x = 1; x <= lab->largeur; ++x)
        {
            start = false;
            for (y = 0, idx = x - 1; y < lab->hauteur; ++y, idx += lab->largeur )
            {
                if (lab->flag[idx] & MUR_D)
                {
                    if (!start)
                    {
                        cairo_move_to (cr, x * SZ_CELL_EXPORT, y * SZ_CELL_EXPORT);
                        start = true;
                    }
                }
                else
                {
                    if (start) // fin d'une ligne, on trace
                    {
                        cairo_line_to(cr, x * SZ_CELL_EXPORT, y * SZ_CELL_EXPORT);
                        start = false;
                    }
                }
            }
            if (start) // fin d'une ligne, on trace
            {
                cairo_line_to(cr, x * SZ_CELL_EXPORT, y * SZ_CELL_EXPORT);
            }
        }


        cairo_set_line_width (cr, 1.5);
        cairo_stroke (cr);
    }

    cairo_surface_write_to_png (surface, outfilename);

    cairo_destroy (cr);
    cairo_surface_destroy (surface);
}
