#include "maze.h"

#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>

using namespace std;

/////////////////////////////////
// Union-Find gestion
/////////////////////////////////

void makeset(uf_t* puf)
{
    puf->parent = puf;
    puf->rank = 0;
}

uf_t* find(uf_t* pufx)
{
    if (pufx->parent != pufx)
        pufx->parent = find(pufx->parent);
    return pufx->parent;
}

void unionset(uf_t* pufx, uf_t* pufy)
{
    uf_t* xroot = find(pufx);
    uf_t* yroot = find(pufy);

    if (xroot == yroot)
        return;
    else if (xroot->rank < yroot->rank)
        xroot->parent = yroot;
    else if (xroot->rank > yroot->rank)
        yroot->parent = xroot;
    else
    {
        yroot->parent = xroot;
        xroot->rank++;
    }
}


/////////////////////////////////////////////
//         maze gestion
/////////////////////////////////////////////
static int CasserMur (Maze* lab, size_t j, unsigned char wall)
{
    int ret = 0;

    switch (wall)
    {
    case MUR_B:
        if (find(&lab->uf[j]) != find(&lab->uf[j+lab->largeur]))
        {
            lab->flag[j] &= ~MUR_B;
            lab->flag[j+lab->largeur] &= ~MUR_H;
            unionset(&lab->uf[j], &lab->uf[j+lab->largeur]);
            ret = 1;
        }
        break;
    case MUR_D:
        if (find(&lab->uf[j]) != find(&lab->uf[j+1]))
        {
            lab->flag[j] &= ~MUR_D;
            lab->flag[j+1] &= ~MUR_G;
            unionset(&lab->uf[j], &lab->uf[j+1]);
            ret = 1;
        }
        break;
    default:
        break;;
    }

    return ret;
}

static void InitMaze (Maze* lab)
{
    for (size_t i = 0; i < lab->largeur*lab->hauteur; ++i)
    {
        lab->flag[i] = FERME;
        makeset(&lab->uf[i]);
    }
}

/******************************************
Generation de labyrinthe, suivant l'algo de
fusion aleatoire de chemins
(http://ilay.org/yann/articles/maze/)
*****************************************/
void GeneratePerfect2D_Maze (Maze* lab)
{
    InitMaze(lab);

    size_t n = lab->largeur * lab->hauteur;

    vector<wall_t> rnd;
    rnd.reserve(2 * n); // pas 100% precis, mais qu'importe ?

    for (size_t y = 0, i = 0; y < lab->hauteur; ++y)
        for (size_t x = 0; x < lab->largeur; ++x, ++i)
        {
            if (y < lab->hauteur - 1)
            {
                rnd.push_back( wall_t(i, MUR_B) );
            }
            if (x < lab->largeur - 1)
            {
                rnd.push_back( wall_t(i, MUR_D) );
            }
        }

    random_shuffle(rnd.begin(), rnd.end());

    size_t i = 0;
    for (vector<wall_t>::const_iterator it = rnd.begin(); it != rnd.end(); ++it)
    {
        if (i == n - 1) break;

        i += CasserMur(lab, it->idx, it->flag);
    }
}

void DeleteMaze (Maze* lab)
{
    if (lab->flag != NULL)
    {
        free(lab->flag), lab->flag = NULL;
    }
    if (lab->info != NULL)
    {
        free(lab->info), lab->info = NULL;
    }
    if (lab->uf != NULL)
    {
        free(lab->uf), lab->uf = NULL;
    }
    if (lab != NULL)
    {
        free(lab), lab = NULL;
    }
}

Maze* CreateMaze (size_t x, size_t y)
{
    Maze* lab = (Maze*) malloc (sizeof *lab);
    if (lab != NULL)
    {
        lab->largeur = x;
        lab->hauteur = y;

        lab->flag = (unsigned char*) malloc (sizeof *(lab->flag) * x * y);
        lab->info = (pathinfo_t*) malloc (sizeof *(lab->info) * x * y);
        lab->uf = (uf_t*) malloc (sizeof *(lab->uf) * x * y);

        if (lab->flag == NULL || lab->info == NULL || lab->uf == NULL)
        {
            DeleteMaze (lab);
        }
    }
    return lab;
}

///////////////////////////////////////////////////
//        pathfinding gestion
///////////////////////////////////////////////////
void ParcoursLargeur (Maze* lab, size_t i, size_t arrivee)
{
    queue<size_t> searchq;

    lab->info[i].visited = true;
    searchq.push(i);

    while ( !searchq.empty() )
    {
        size_t j = searchq.front();

        if (i == arrivee)
        {
            break;
        }

        /* liste possibilites */
        unsigned char flag = lab->flag[j];

        if ( !(flag&MUR_G) && !lab->info[j-1].visited )
        {
            lab->info[j-1].prev_idx = j;
            lab->info[j-1].visited = true;
            searchq.push(j-1);
        }
        if ( !(flag&MUR_D) && !lab->info[j+1].visited )
        {
            lab->info[j+1].prev_idx = j;
            lab->info[j+1].visited = true;
            searchq.push(j+1);
        }
        if ( !(flag&MUR_H) && !lab->info[j-lab->largeur].visited )
        {
            lab->info[j-lab->largeur].prev_idx = j;
            lab->info[j-lab->largeur].visited = true;
            searchq.push(j-lab->largeur);
        }
        if ( !(flag&MUR_B) && !lab->info[j+lab->largeur].visited )
        {
            lab->info[j+lab->largeur].prev_idx = j;
            lab->info[j+lab->largeur].visited = true;
            searchq.push(j+lab->largeur);
        }

        searchq.pop();
    }
}

static void MarqueChemin(Maze* lab, size_t depart, size_t arrivee)
{
    for (size_t id = arrivee; id != depart; id = lab->info[id].prev_idx )
    {
        lab->info[id].path = true;
    }
}

void ResetInfos (Maze* lab)
{
    for (size_t i = 0; i < lab->largeur*lab->hauteur; i++)
    {
        lab->info[i].path = false;
        lab->info[i].visited = false;
        lab->info[i].prev_idx = i;  // => undefined
    }
}

bool RechercheChemin (Maze* lab, size_t depart, size_t arrivee)
{
    ResetInfos(lab);

    ParcoursLargeur (lab, depart, arrivee);

    /* Si l'arrivee a ete atteinte */
    if (lab->info[arrivee].prev_idx != arrivee)
    {
        MarqueChemin(lab, depart, arrivee);
        return true;
    }
    else return false;
}
