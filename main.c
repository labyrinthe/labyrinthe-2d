#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <SDL/SDL.h>

#include "laby.h"
#include "affichage.h"


static void ChargeImages (SDL_Surface** bmpLaby, char** bmpName)
{
    int i;
    for (i=0; i<NB_BITMAPS; i++)
    {
        bmpLaby[i] = SDL_LoadBMP(bmpName[i]);
        if (bmpLaby[i] == NULL)
            fprintf(stderr, "unable to load %s.\n", bmpName[i]);
    }

    SDL_SetAlpha(BMP_CHM, SDL_SRCALPHA, 128);
    SDL_SetAlpha(BMP_PLY, SDL_SRCALPHA, 128);
    /* passe les coins de l'arrivee en transparent */
    SDL_SetColorKey(BMP_ARRIVEE, SDL_SRCCOLORKEY, SDL_MapRGB(BMP_ARRIVEE->format, 255, 255, 255));
}

static void FreeImages (SDL_Surface** bmp)
{
    int i;
    for (i=0; i<NB_BITMAPS; i++)
    {
        SDL_FreeSurface(bmp[i]);
    }
}

static void ConfigureLabyrinthe (char* arg, size_t* x, size_t *y)
{
    if (!strcmp(arg, "-easy"))
    {
        *x /= 1.5, *y /= 1.5;
    }
    else if (!strcmp(arg, "-hard"))
    {
        *x *= 1.5, *y *= 1.5;
    }
    else if (!strcmp(arg, "-expert"))
    {
        *x *= 2, *y *= 2;
    }
}

void ConfigureModeVideo (char* arg, Uint32* flagVideo)
{
    if (!strcmp(arg, "-fullscreen"))
    {
        *flagVideo |= SDL_FULLSCREEN;
    }
}

#define DEFAULT_X   600
#define DEFAULT_Y   400

int main (int argc, char *argv[])
{
    size_t x = DEFAULT_X, y = DEFAULT_Y, arrivee = 0;
    Laby *lab = NULL;
    clock_t t = 0;
    SDL_Rect positionJoueur, positionArrivee;
    int continuer = 1;
    SDL_Event event;
    Uint32 flagVideo = SDL_HWSURFACE | SDL_DOUBLEBUF;
    char *bmpNames[] = {  "joueur.bmp",
                          "chemin.bmp",
                          "mur_gauche.bmp",
                          "mur_haut.bmp",
                          "mur_gauche&mur_haut.bmp",
                          "sans_mur.bmp",
                          "arrivee.bmp"
                       };

    srand ((unsigned)time(NULL));

    /* recuperation et traitement des parametres */
    if (argc > 1)
    {
        ConfigureLabyrinthe (argv[1], &x, &y);
    }
    if (argc > 2)
    {
        ConfigureModeVideo (argv[2], &flagVideo);
    }

    /* Initialise la SDL */
//    SDL_Init(SDL_INIT_VIDEO);
//    ecran = SDL_SetVideoMode(x*SZ_CELL, y*SZ_CELL, 32, flagVideo);
//    SDL_WM_SetCaption("my_lab", NULL);
//    ChargeImages (bmpLaby, bmpNames);

    /* Initialise le labyrinthe et les positions */
    if ( (lab = CreeLabyrinthe (x, y)) != NULL )
    {
genereLaby:
        t = clock();
        GenereLabyrintheParfait_2D (lab);
        fprintf(stderr, "generation : %.2fs.\n", (double)(clock()-t) / CLOCKS_PER_SEC);

        ExportTo (lab, "test.png");
exit(0);
        /* arrivee au coin inferieur droit */
        arrivee = (x*y)-1;
        positionArrivee.x = (arrivee%lab->largeur)*SZ_CELL;
        positionArrivee.y = (arrivee/lab->largeur)*SZ_CELL;

        /* joueur au coin superieur gauche */
        positionJoueur.x = 0, positionJoueur.y = 0;
        AffichageLabyrinthe (lab, &positionJoueur, &positionArrivee);


        while (continuer)
        {
            SDL_Flip(ecran);
            SDL_WaitEvent(&event);
            switch (event.type)
            {
            case SDL_QUIT:
                continuer = 0;
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_UP:    // Fl�che haut
                    DeplacerJoueur (lab, &positionJoueur, N);
                    break;
                case SDLK_DOWN:  // Fl�che bas
                    DeplacerJoueur (lab, &positionJoueur, S);
                    break;
                case SDLK_RIGHT: // Fl�che droite
                    DeplacerJoueur (lab, &positionJoueur, E);
                    break;
                case SDLK_LEFT:  // Fl�che gauche
                    DeplacerJoueur (lab, &positionJoueur, W);
                    break;
                case SDLK_F1:    // Aide
                    RechercheChemin (lab, positionJoueur.x/SZ_CELL+((positionJoueur.y/SZ_CELL)*lab->largeur), arrivee);
                    AffichageChemin (lab, &positionJoueur, &positionArrivee);
                    break;
                case SDLK_F2:    // Refresh
                    goto genereLaby;
                    break;
                case SDLK_F3:
                    directionnal_fill(lab, positionJoueur.x/SZ_CELL+((positionJoueur.y/SZ_CELL)*lab->largeur));
                    AffichageChemin (lab, &positionJoueur, &positionArrivee);
                    break;
                case SDLK_ESCAPE:
                    continuer = 0;
                    break;
                default:
                    break;
                }
                break;
            }
        }

        SupprimeLabyrinthe (lab);
    }
    FreeImages (bmpLaby);
    SDL_Quit();

    return EXIT_SUCCESS;
}
