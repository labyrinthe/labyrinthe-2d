#ifndef LABY_H
#define LABY_H

#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>

#include "unionfind.h"


#define MUR_G 1
#define MUR_D 2
#define MUR_H 4
#define MUR_B 8
#define FERME (MUR_G|MUR_D|MUR_H|MUR_B)


/*
typedef struct
{
    unsigned char flag;
    unsigned char chemin;
    uf_t uf;
} case_t;

typedef struct
{
    size_t largeur, hauteur;
    case_t* cases;
} Laby;
*/


/* Structure contenant les parametres du labyrinthe */
typedef struct
{
    size_t largeur, hauteur;
    unsigned char* flag;     /* etat de la cellule (murs) */
    unsigned char* chemin;   /* BOOL ; si chemin : 1, sinon : 0*/
    uf_t* uf;     /* enregistre si la cellule est reliee au labyrinthe */
} Laby;

/* Structure pour effectuer la recherche du plus court chemin */
typedef struct
{
    size_t* prec_id;    /* id de la case precedente la plus proche */
    size_t* file;       /* file pour parcours en largeur */
    char* visited;      /* BOOL ; si visite : 1, sinon : 0 */
}
T_Search;


Laby* CreeLabyrinthe (size_t x, size_t y);
void GenereLabyrintheParfait_2D (Laby* lab);
void SupprimeLabyrinthe (Laby* lab);

int RechercheChemin (Laby* lab, size_t depart, size_t arrivee);
void InitChemin (Laby* lab);

void directionnal_fill(Laby* lab, size_t j);

#endif
