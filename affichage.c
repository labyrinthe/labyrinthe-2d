#include <stdlib.h>


#include "laby.h"
#include "affichage.h"

/***********************************************
fonction d'affichage d'une cellule du labyrinthe
************************************************/
static void AffichageCellule (Laby* lab, SDL_Rect* positionRect, size_t idx)
{
    if (lab->flag[idx]&MUR_G && lab->flag[idx]&MUR_H)
    {
        SDL_BlitSurface(BMP_MUR_G_H, NULL, ecran, positionRect);
    }
    else if (lab->flag[idx]&MUR_G)
    {
        SDL_BlitSurface(BMP_MUR_G, NULL, ecran, positionRect);
    }
    else if (lab->flag[idx]&MUR_H)
    {
        SDL_BlitSurface(BMP_MUR_H, NULL, ecran, positionRect);
    }
    else
    {
        SDL_BlitSurface(BMP_NO_MUR, NULL, ecran, positionRect);
    }
}

/********************************
gestion de deplacement du joueur
*********************************/
void DeplacerJoueur (Laby* lab, SDL_Rect* positionJoueur, Direction direction)
{
    size_t idx = positionJoueur->x/SZ_CELL+((positionJoueur->y/SZ_CELL)*lab->largeur);
    /* Efface le joueur de la position actuelle */
    AffichageCellule (lab, positionJoueur, idx);
    /* Verifie si le deplacement est possible */
    if (direction == W)
    {
        if ( !(lab->flag[idx]&MUR_G) )
        {
            /* deplace le joueur */
            positionJoueur->x -= SZ_CELL;
        }
    }
    else if (direction == E)
    {
        if ( !(lab->flag[idx]&MUR_D) )
        {
            positionJoueur->x += SZ_CELL;
        }
    }
    else if (direction == N)
    {
        if ( !(lab->flag[idx]&MUR_H) )
        {
            positionJoueur->y -= SZ_CELL;
        }
    }
    else
    {
        if ( !(lab->flag[idx]&MUR_B) )
        {
            positionJoueur->y += SZ_CELL;
        }
    }
    /* blit du joueur a la nouvelle position */
    SDL_BlitSurface(BMP_PLY, NULL, ecran, positionJoueur);
}

void AffichageChemin (Laby* lab, SDL_Rect* positionJoueur, SDL_Rect* positionArrivee)
{
    size_t idx;
    SDL_Rect positionRect;

    for (idx = 0, positionRect.y=0; (unsigned) positionRect.y < lab->hauteur*SZ_CELL; positionRect.y += SZ_CELL)
    {
        for (positionRect.x=0; (unsigned) positionRect.x < lab->largeur*SZ_CELL; positionRect.x += SZ_CELL, idx++)
        {
            if (lab->chemin[idx])
            {
                SDL_BlitSurface(BMP_CHM, NULL, ecran, &positionRect);
            }
        }
    }
    SDL_Flip(ecran);
    /* Attends une seconde... */
    SDL_Delay(1000);
    /* ...puis efface le chemin */
    AffichageLabyrinthe (lab, positionJoueur, positionArrivee);
}

void AffichageLabyrinthe (Laby* lab, SDL_Rect* positionJoueur, SDL_Rect* positionArrivee)
{
    size_t idx;
    SDL_Rect positionRect;

    for (idx = 0, positionRect.y=0; (unsigned) positionRect.y < lab->hauteur*SZ_CELL; positionRect.y += SZ_CELL)
    {
        for (positionRect.x=0; (unsigned) positionRect.x < lab->largeur*SZ_CELL; positionRect.x += SZ_CELL, idx++)
        {
            AffichageCellule (lab, &positionRect, idx);
        }
    }
    SDL_BlitSurface(BMP_PLY, NULL, ecran, positionJoueur);
    SDL_BlitSurface(BMP_ARRIVEE, NULL, ecran, positionArrivee);
}
