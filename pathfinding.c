#include <stdlib.h>

#include "laby.h"

#define TRUE 1


void ParcoursLargeur (Laby* lab, T_Search* t_search, size_t j, size_t arrivee)
{
    unsigned char flag;
    /* i : indice de position dans la file
       avant i : closed-list; apres i : open-list */
    size_t i = 0;
    /* n : nb de cases dans la file */
    size_t n = 0;
    t_search->file[i] = j;
    t_search->visited[j] = TRUE;
    while (i <= n)
    {
        /* liste possibilites */
        flag = lab->flag[j];
        if ( !(flag&MUR_G) && !(t_search->visited[j-1]) )
        {
            t_search->prec_id[j-1] = j;
            t_search->visited[j-1] = TRUE;
            /* Ajoute cellule a la file */
            t_search->file[++n] = j-1;
            if (j-1 == arrivee)
                break;
        }
        if ( !(flag&MUR_D) && !(t_search->visited[j+1]) )
        {
            t_search->prec_id[j+1] = j;
            t_search->visited[j+1] = TRUE;
            t_search->file[++n] = j+1;
            if (j+1 == arrivee)
                break;
        }
        if ( !(flag&MUR_H) && !(t_search->visited[j-lab->largeur]) )
        {
            t_search->prec_id[j-lab->largeur] = j;
            t_search->visited[j-lab->largeur] = TRUE;
            t_search->file[++n] = j-lab->largeur;
            if (j-lab->largeur == arrivee)
                break;
        }
        if ( !(flag&MUR_B) && !(t_search->visited[j+lab->largeur]) )
        {
            t_search->prec_id[j+lab->largeur] = j;
            t_search->visited[j+lab->largeur] = TRUE;
            t_search->file[++n] = j+lab->largeur;
            if (j+lab->largeur == arrivee)
                break;
        }
        /* passe au suivant dans la open-liste */
        j = t_search->file[++i];
    }
}

static void MarqueChemin(Laby* lab, size_t* prec_id, size_t depart, size_t arrivee)
{
    size_t id;
    for ( id = arrivee; id != depart; id = prec_id[id] )
    {
        lab->chemin[id] = 1;
    }
}

static void TermineRecherche (T_Search* t_search)
{
    if (t_search->file != NULL)
    {
        free (t_search->file), t_search->file = NULL;
    }
    if (t_search->visited != NULL)
    {
        free (t_search->visited), t_search->visited = NULL;
    }
    if (t_search->prec_id != NULL)
    {
        free (t_search->prec_id), t_search->prec_id = NULL;
    }
}

static int InitTabs (T_Search* t_search, size_t size)
{
    int ret = 0;
    size_t i;
    t_search->file = malloc(sizeof *(t_search->file) * size);
    t_search->visited = malloc(sizeof *(t_search->visited) * size);
    t_search->prec_id = malloc(sizeof *(t_search->prec_id) * size);

    if ( t_search->file != NULL && t_search->visited != NULL && t_search->prec_id != NULL )
    {
        ret = 1;
        for (i=0; i<size; i++)
        {
            t_search->prec_id[i] = i;       /* UNITIALIZED */
            t_search->file[i] = 0;          /* UNITIALIZED */
            t_search->visited[i] = 0;       /* FALSE */
        }
    }
    else
    {
        TermineRecherche (t_search);
    }
    return ret;
}

void InitChemin (Laby* lab)
{
    size_t i;
    for (i=0; i<lab->largeur*lab->hauteur; i++)
    {
        lab->chemin[i] = 0;
    }
}

int RechercheChemin (Laby* lab, size_t depart, size_t arrivee)
{
    /* valeur de retour - FALSE */
    int ret = 0;
    T_Search t_search;
    size_t size = lab->largeur* lab->hauteur;

    if ( InitTabs (&t_search, size) )
    {
        InitChemin(lab);

        ParcoursLargeur (lab, &t_search, depart, arrivee);
        /* Si l'arrivee a ete atteinte */
        if (t_search.prec_id[arrivee] != arrivee)
        {
            /* valeur de retour - TRUE */
            ret = 1;
            MarqueChemin(lab, t_search.prec_id, depart, arrivee);
        }
        TermineRecherche (&t_search);
    }
    return ret;
}
