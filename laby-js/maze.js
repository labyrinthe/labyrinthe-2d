function makeset(obj) {
  obj.parent = obj;
  obj.rank = 0;
}

function find(obj) {
  if (obj != obj.parent) {
    obj.parent = find(obj.parent);
  }
  return obj.parent;
}

function union(obj1, obj2)
{
  var xroot = find(obj1);
  var yroot = find(obj2);

  if (xroot == yroot)
    return;
  else if (xroot.rank < yroot.rank)
    xroot.parent = yroot;
  else if (xroot.rank > yroot.rank)
    yroot.parent = xroot;
  else {
    yroot.parent = xroot;
    xroot.rank++;
  }
}

var cell_sz = 10;
var maze;

function getCell(x, y, side) {
  return {
    walls : Array(1,1,1,1),
    visited : false,
    path : false,
  };
}

function getWall(x, y, side) {
  return {
    x : x,
    y : y,
    side : side
  };
}

function breakWall(x, y, maze, side) {
  if (side == 0) {  // left side
    if (find(maze[y][x]) != find(maze[y][x-1])) {
      maze[y][x].walls[0] = maze[y][x-1].walls[1] = 0;
      union(maze[y][x],maze[y][x-1]);
      return 1;
    }
  } else if (side == 2) {  // up side
    if (find(maze[y][x]) != find(maze[y-1][x])) {
      maze[y][x].walls[2] = maze[y-1][x].walls[3] = 0;
      union(maze[y][x],maze[y-1][x]);
      return 1;
    }
  }
  return 0;
}

function makeMaze(w, h) {
  m = new Array;
  for (var i=0; i < h; i++) {
    m.push(new Array);
    for (var j=0; j < w; j++) {
      var cell = new getCell();
      m[i].push(cell);
    }
  }
  return m;
}

//+ Jonas Raoni Soares Silva @ http://jsfromhell.com/array/shuffle [v1.0]
function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};

function generate1(w, h) {
  maze = makeMaze(w, h);
  //
  var walls = new Array;
  for (var i=0; i < h; i++) {
    for (var j=0; j < w; j++) {
      makeset(maze[i][j]);
      //
      if (j > 0)
        walls.push(new getWall(j,i,0));  // left side
      if (i > 0)
        walls.push(new getWall(j,i,2));  // up side
    }
  }
  //
  walls = shuffle(walls);
  //
  var i = 0, n = w * h;
  for (var j=0; j < walls.length; j++) {
    if (i == n - 1) break;
    i += breakWall(walls[j].x, walls[j].y, maze, walls[j].side);
  }
  //
  return maze;
}

function generate2(w, h) {
  //
  maze = makeMaze(w, h);
  // 
  var stack = [{x:0,y:0}];
  maze[0][0].set = true;
  
  while(stack.length > 0) {
    var pos = stack[stack.length-1];
    
    var candidates = [];
    if (pos.x > 0 && !maze[pos.y][pos.x-1].set) {
      candidates.push({x:pos.x-1,y:pos.y,w1:0,w2:1});
    }
    if (pos.x + 1 < w && !maze[pos.y][pos.x+1].set) {
      candidates.push({x:pos.x+1,y:pos.y,w1:1,w2:0});
    }
    if (pos.y > 0 && !maze[pos.y-1][pos.x].set) {
      candidates.push({x:pos.x,y:pos.y-1,w1:2,w2:3});
    }
    if (pos.y + 1 < h && !maze[pos.y+1][pos.x].set) {
      candidates.push({x:pos.x,y:pos.y+1,w1:3,w2:2});
    }
    //
    if (candidates.length > 0) {
      var i = Math.floor(Math.random()*candidates.length);
      var candidate = candidates[i];
      //
      maze[pos.y][pos.x].walls[candidate.w1] = 0;
      maze[candidate.y][candidate.x].walls[candidate.w2] = 0;
      maze[candidate.y][candidate.x].set = true;
      stack.push({ x:candidate.x, y:candidate.y });
    }
    else {
      stack.pop();
    }
  }

  return maze;
}

function pathfinding(maze, depart, arrivee) {
  var x = depart[0], y = depart[1];
  var queue = [];
  
  queue.push([x,y]);
  maze[y][x].visited = true;
  while (queue.length > 0) {
    var current = queue.shift();
    x = current[0], y = current[1];

    if (x == arrivee[0] && y == arrivee[1])
      break;

    if (!maze[y][x].walls[0] && !maze[y][x-1].visited) {
      maze[y][x-1].prev = [x,y];
      maze[y][x-1].visited = true;
      queue.push([x-1,y]);
    }
    if (!maze[y][x].walls[1] && !maze[y][x+1].visited) {
      maze[y][x+1].prev = [x,y];
      maze[y][x+1].visited = true;
      queue.push([x+1,y]);
    }
    if (!maze[y][x].walls[2] && !maze[y-1][x].visited) {
      maze[y-1][x].prev = [x,y];
      maze[y-1][x].visited = true;
      queue.push([x,y-1]);
    }
    if (!maze[y][x].walls[3] && !maze[y+1][x].visited) {
      maze[y+1][x].prev = [x,y];
      maze[y+1][x].visited = true;
      queue.push([x,y+1]);
    }
  }
  // check if path found
  x = arrivee[0], y = arrivee[1];
  if (maze[y][x].prev == null)
    return false;
  // mark path
  while (x != depart[0] || y != depart[1]) {
    maze[y][x].path = true;
    var prev = maze[y][x].prev;
    x = prev[0];
    y = prev[1];
  }
  maze[y][x].path = true;
  return true;
}

function drawCell(ctx, walls, x, y) { 
  if (walls[0]) {  // left side
    ctx.moveTo(x*cell_sz,y*cell_sz);
    ctx.lineTo(x*cell_sz,y*cell_sz+cell_sz);
  }
  if (walls[2]) {  // up side
    ctx.moveTo(x*cell_sz,y*cell_sz);
    ctx.lineTo(x*cell_sz+cell_sz,y*cell_sz);
  }
}

function drawing(w, h) {
  var canvas = document.getElementById("canvas");
  var ctx = canvas.getContext("2d");
  
  canvas.width  = cell_sz * w;
  canvas.height = cell_sz * h;
  // start drawing
  for (var y=0; y < h; y++) {
    for (var x=0; x < w; x++) {
      drawCell(ctx, maze[y][x].walls, x, y);
    }
  }
  ctx.moveTo(0,h*cell_sz);
  ctx.lineTo(w*cell_sz,h*cell_sz);
  ctx.moveTo(w*cell_sz,0);
  ctx.lineTo(w*cell_sz,h*cell_sz);

  ctx.stroke();
  // complete drawing 
}

function draw() {
  var w = parseInt(document.getElementById("width").value);
  var h = parseInt(document.getElementById("height").value);
  
  
  var d = new Date();
  if (document.getElementById("gen_method1").checked) {
    maze = generate1(w, h);
  } else if (document.getElementById("gen_method2").checked) {
    maze = generate2(w, h);
  }
  var tm = new Date() - d;
  
  document.getElementById("chrono").innerHTML = "&nbsp;generated in " + tm.toString() + "ms";
  
  maze.w = w;
  maze.h = h;
  
  drawing(w, h);
}

function drawPath() {
  var w = maze.w;
  var h = maze.h;

  var s = pathfinding(maze, [0,0], [w-1, h-1]);
  if (s) {
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");

    // start drawing
    for (var y=0; y < h; y++) {
      for (var x=0; x < w; x++) {
        if (maze[y][x].path) {
          ctx.rect(x*cell_sz+1,y*cell_sz+1,cell_sz-2,cell_sz-2);
        }
      }
    }
    ctx.fillStyle="yellow";
    ctx.fill();
    // complete drawing 
  }
}
