#ifndef AFFICHAGE_H
#define AFFICHAGE_H

#include <SDL/SDL.h>

#include "laby.h"

#define SZ_CELL 10



#define NB_BITMAPS 7
#define BMP_PLY      bmpLaby[0]
#define BMP_CHM      bmpLaby[1]
#define BMP_MUR_G    bmpLaby[2]
#define BMP_MUR_H    bmpLaby[3]
#define BMP_MUR_G_H  bmpLaby[4]
#define BMP_NO_MUR   bmpLaby[5]
#define BMP_ARRIVEE  bmpLaby[6]

/* Va. globales */
SDL_Surface *ecran;
SDL_Surface *bmpLaby[NB_BITMAPS];

typedef enum { W, E, N, S } Direction;

void DeplacerJoueur (Laby* lab, SDL_Rect* positionJoueur, Direction direction);
void AffichageLabyrinthe (Laby* lab, SDL_Rect* positionJoueur, SDL_Rect* positionArrivee);
void AffichageChemin (Laby* lab, SDL_Rect* positionJoueur, SDL_Rect* positionArrivee);
void ExportTo (Laby* lab, const char* outfilename);

#endif
