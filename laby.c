#include <stdlib.h>
#include <stdio.h>

#include "laby.h"
#include "aleatoire.h"


/*************************************************
fonction qui parcours les cases reliees a j
(parcours en profondeur) et leur donne l'id voulu
**************************************************/

static void CasserMur (Laby* lab, size_t j, int mur_a_casser)
{
    lab->flag[j] &= ~mur_a_casser;

    if (mur_a_casser == MUR_G)
    {
        lab->flag[j-1] &= ~MUR_D;
        unionset(&lab->uf[j], &lab->uf[j-1]);
    }
    else if (mur_a_casser == MUR_D)
    {
        lab->flag[j+1] &= ~MUR_G;
        unionset(&lab->uf[j], &lab->uf[j+1]);
    }
    else if (mur_a_casser == MUR_H)
    {
        lab->flag[j-lab->largeur] &= ~MUR_B;
        unionset(&lab->uf[j], &lab->uf[j-lab->largeur]);
    }
    else if (mur_a_casser == MUR_B)
    {
        lab->flag[j+lab->largeur] &= ~MUR_H;
        unionset(&lab->uf[j], &lab->uf[j+lab->largeur]);
    }
    else return;
}

/**
fonction "principale"
*/
static int FormateCell (Laby* lab, size_t j)
{
    size_t x = j % lab->largeur;
    size_t y = j / lab->largeur;

    unsigned nb_possibilites = 0;
    unsigned possibilite[4] = { 0 };
    int mur_a_casser = 0;

    uf_t* uf = find(&lab->uf[j]);

    /* si la case n'est pas en bord du labyrinthe
       et si la case voisine n'est pas connectée */
    if (x > 0 && uf != find(&lab->uf[j-1]))
    {
        /* on ajoute le mur au tableau des possibilites */
        possibilite[nb_possibilites++] = MUR_G;
    }
    if (x < lab->largeur-1 && uf != find(&lab->uf[j+1]))
    {
        possibilite[nb_possibilites++] = MUR_D;
    }
    if (y > 0 && uf != find(&lab->uf[j-lab->largeur]))
    {
        possibilite[nb_possibilites++] = MUR_H;
    }
    if (y < lab->hauteur-1 && uf != find(&lab->uf[j+lab->largeur]))
    {
        possibilite[nb_possibilites++] = MUR_B;
    }

    /* s'il existe un mur a casser */
    if (nb_possibilites > 0)
    {
        /* On choisit aleatoirement le mur a casser */
        mur_a_casser = possibilite[rand() % nb_possibilites];
        CasserMur (lab, j, mur_a_casser);
        /* on retourne que l'on a casse un mur */
        return 1;
    }
    /* sinon on n'a rien casse */
    else return 0;
}

static void InitLabyrinthe (Laby* lab)
{
    size_t i;
    for (i=0; i<lab->largeur*lab->hauteur; i++)
    {
        lab->flag[i] = FERME;
        makeset(&lab->uf[i]);
    }
}

/******************************************
Generation de labyrinthe, suivant l'algo de
fusion aleatoire de chemins
(http://ilay.org/yann/articles/maze/)
*****************************************/
void GenereLabyrintheParfait_2D (Laby* lab)
{
    size_t i = 0;
    /* utilise un generateur de nombre aleatoires uniques
       pour une generation plus rapide */
    if ( InitGenerateur (0, (lab->largeur*lab->hauteur)-1) )
    {
        /* initialise les id,
           et met toutes les cases a FERME */
        InitLabyrinthe (lab);
        /* tant qu'il reste des murs a casser */
        while ( i < (lab->largeur*lab->hauteur)-1 )
        {
            i += FormateCell( lab, (unsigned) ReturnAleatoire ());
        }
        TermineGenerateur();
    }
}

void SupprimeLabyrinthe (Laby* lab)
{
    if (lab->flag != NULL)
    {
        free(lab->flag), lab->flag = NULL;
    }
    if (lab->chemin != NULL)
    {
        free(lab->chemin), lab->chemin = NULL;
    }
    if (lab->uf != NULL)
    {
        free(lab->uf), lab->uf = NULL;
    }
    if (lab != NULL)
    {
        free(lab), lab = NULL;
    }
}

Laby* CreeLabyrinthe (size_t x, size_t y)
{
    Laby* lab = malloc (sizeof *lab);
    if (lab != NULL)
    {
        lab->largeur = x;
        lab->hauteur = y;

        lab->flag = malloc (sizeof *(lab->flag) * x * y);
        lab->uf = malloc (sizeof *(lab->uf) * x * y);
        lab->chemin = malloc (sizeof *(lab->chemin) * x * y);

        if (lab->flag == NULL || lab->chemin == NULL || lab->uf == NULL)
        {
            SupprimeLabyrinthe (lab);
        }
    }
    return lab;
}
