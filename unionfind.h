#ifndef UNIONFIND_H_
#define UNIONFIND_H_

#include <stddef.h>

/* union_find */
typedef struct
{
    void* parent;
    size_t rank;
} uf_t;


void makeset(uf_t* puf);
void unionset(uf_t* pufx, uf_t* pufy);
uf_t* find(uf_t* pufx);


#endif // UNIONFIND_H_
